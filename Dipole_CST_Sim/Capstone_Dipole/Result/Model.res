MWS Result File Version 20150206
size=i:93

type=s:DATA_FOLDER
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:rebuild
result=s:1
files=s:raw_data

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:rebuild
result=s:1
files=s:$SIMonItem$_1_0.s3d

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:rebuild
result=s:1
files=s:$SIMonInfo$.sin

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:rebuild
result=s:1
files=s:RefSpectrum_1.sig

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:rebuild
result=s:1
files=s:e-field (f=900)_1,1.m3d

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:rebuild
result=s:1
files=s:World.fid

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:survivemeshadapt
result=s:1
files=s:model.gex

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:survivemeshadapt
result=s:1
files=s:PP.fmm

type=s:HIDDENITEM
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:persistent
result=s:0
files=s:MCalcAccess.log

type=s:FOLDER
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Discrete Ports\Voltages\Signals\Port 1 [1]
files=s:pus1(1).sig
xlabel=s:Time / ns
title=s:Discrete Port Voltage Time Signals

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Discrete Ports\Currents\Signals\Port 1 [1]
files=s:pis1(1).sig
xlabel=s:Time / ns
title=s:Discrete Port Current Time Signals

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Discrete Ports\Voltages\Port 1 [1]
files=s:puc1(1).sig
xlabel=s:Frequency / MHz
title=s:Discrete Port Voltage

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Discrete Ports\Currents\Port 1 [1]
files=s:pic1(1).sig
xlabel=s:Frequency / MHz
title=s:Discrete Port Current

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Discrete Ports\Impedances\Port 1 [1]
files=s:pzc1(1).sig
xlabel=s:Frequency / MHz
title=s:Discrete Port Impedance

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Port signals\i1
files=s:i1(1).sig
xlabel=s:Time / ns
title=s:Time Signals

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Port signals\o1,1
files=s:o1(1)1(1).sig
xlabel=s:Time / ns
title=s:Time Signals

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\S-Parameters\S1,1
files=s:cS1(1)1(1).sig
xlabel=s:Frequency / MHz
title=s:S-Parameters

type=s:XYSIGNAL2
subtype=s:balance
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Balance\Balance [1]
files=s:1.bil

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Power Stimulated
files=s:StimulatedPower_1.sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Power Outgoing all Ports
files=s:ReflectedPower_1.sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Power Accepted
files=s:AcceptedPower_1.sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Power Accepted per Port\Port 1
files=s:AcceptedPower_1(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:energy
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Energy\Energy [1]
files=s:1.eng
xlabel=s:Time / ns
title=s:Field Energy / dB

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Y Matrix\Y1,1
files=s:ycmplx1(1)1(1).sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Z Matrix\Z1,1
files=s:zcmplx1(1)1(1).sig

type=s:XYSIGNAL2
subtype=s:linear
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\VSWR\VSWR1
files=s:vswr1(1).sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Power Radiated
files=s:RadiatedPower_1.sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Efficiencies\Rad. Efficiency [1]
files=s:FarfieldMetaData_1_RadEff.sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Efficiencies\Tot. Efficiency [1]
files=s:FarfieldMetaData_1_TotEff.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=0\farfield (f=800)
files=s:Farfield_Cut_farfield (f=800)_Phi=0_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=90\farfield (f=800)
files=s:Farfield_Cut_farfield (f=800)_Phi=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Theta=90\farfield (f=800)
files=s:Farfield_Cut_farfield (f=800)_Theta=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=0\farfield (f=900)
files=s:Farfield_Cut_farfield (f=900)_Phi=0_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=90\farfield (f=900)
files=s:Farfield_Cut_farfield (f=900)_Phi=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Theta=90\farfield (f=900)
files=s:Farfield_Cut_farfield (f=900)_Theta=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=0\farfield (f=1000)
files=s:Farfield_Cut_farfield (f=1000)_Phi=0_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=90\farfield (f=1000)
files=s:Farfield_Cut_farfield (f=1000)_Phi=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Theta=90\farfield (f=1000)
files=s:Farfield_Cut_farfield (f=1000)_Theta=90_[1]_0.sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Reference Impedance\ZRef 1(1)
files=s:ZRef1(1).sig

type=s:RESULT_0D
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\AutomaticRunInformation
files=s:AutomaticRunInformation

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Brass (65%)\Surface Impedance\Z' (Fit)
files=s:Brass (65%)_Z_re.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Brass (65%)\Surface Impedance\Z'' (Fit)
files=s:Brass (65%)_Z_im.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Brass (65%)\Surface Impedance\Z' (Theory)
files=s:Brass (65%)_Z_datalist_re.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Brass (65%)\Surface Impedance\Z'' (Theory)
files=s:Brass (65%)_Z_datalist_im.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Copper (annealed)\Surface Impedance\Z' (Fit)
files=s:Copper (annealed)_Z_re.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Copper (annealed)\Surface Impedance\Z'' (Fit)
files=s:Copper (annealed)_Z_im.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Copper (annealed)\Surface Impedance\Z' (Theory)
files=s:Copper (annealed)_Z_datalist_re.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:1D Results\Materials\Copper (annealed)\Surface Impedance\Z'' (Theory)
files=s:Copper (annealed)_Z_datalist_im.sig
xlabel=s:Frequency / MHz
ylabel=s:Complex impedance / Ohm/sq
title=s:Lossy metal surface impedance (Vacuum filling): Nth Order Model, N=1 (Fit)

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:Low Frequency:4:3
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:Excitation Signals\default
files=s:signal_default_lf.sig
xlabel=s:Time / ns
title=s:Excitation: default

type=s:XYSIGNAL2
subtype=s:user
problemclass=s:High Frequency:0:0
visibility=s:visible
creation=s:internal
lifetime=s:persistent
result=s:0
treepath=s:Excitation Signals\default
files=s:signal_default.sig
xlabel=s:Time / ns
title=s:Excitation: default

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Discrete Ports\Voltages\Signals\Port 1 [1]
files=s:pus1(1).sig
xlabel=s:Time / ns
title=s:Discrete Port Voltage Time Signals

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Discrete Ports\Currents\Signals\Port 1 [1]
files=s:pis1(1).sig
xlabel=s:Time / ns
title=s:Discrete Port Current Time Signals

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Discrete Ports\Voltages\Port 1 [1]
files=s:puc1(1).sig
xlabel=s:Frequency / MHz
title=s:Discrete Port Voltage

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Discrete Ports\Currents\Port 1 [1]
files=s:pic1(1).sig
xlabel=s:Frequency / MHz
title=s:Discrete Port Current

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Discrete Ports\Impedances\Port 1 [1]
files=s:pzc1(1).sig
xlabel=s:Frequency / MHz
title=s:Discrete Port Impedance

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Loss per Material\Metal loss in Brass (65%)
files=s:cMetal_loss_Brass (65%)(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Loss per Material\Metal loss in Copper (annealed)
files=s:cMetal_loss_Copper (annealed)(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Loss in Metals
files=s:cTotal_metal_loss(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:EFIELD3D
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:2D/3D Results\E-Field\e-field (f=900) [1]
files=s:e-field (f=900)_1,1.m3d
files=s:e-field (f=900)_1,1_m3d.rex
ylabel=s:e-field (f=900) [1]

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Port signals\i1
files=s:i1(1).sig
xlabel=s:Time / ns
title=s:Time Signals

type=s:XYSIGNAL2
subtype=s:time
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Port signals\o1,1
files=s:o1(1)1(1).sig
xlabel=s:Time / ns
title=s:Time Signals

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\S-Parameters\S1,1
files=s:cS1(1)1(1).sig
xlabel=s:Frequency / MHz
title=s:S-Parameters

type=s:XYSIGNAL2
subtype=s:balance
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Balance\Balance [1]
files=s:1.bil

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Power Stimulated
files=s:StimulatedPower_1.sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Power Outgoing all Ports
files=s:ReflectedPower_1.sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Power Accepted
files=s:AcceptedPower_1.sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Power Accepted per Port\Port 1
files=s:AcceptedPower_1(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:energy
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Energy\Energy [1]
files=s:1.eng
xlabel=s:Time / ns
title=s:Field Energy / dB

type=s:FARFIELD
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\farfield (f=800) [1]
files=s:farfield (f=800)_1.ffm
ylabel=s:farfield (f=800) [1]

type=s:FARFIELD
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\farfield (f=900) [1]
files=s:farfield (f=900)_1.ffm
ylabel=s:farfield (f=900) [1]

type=s:FARFIELD
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\farfield (f=1000) [1]
files=s:farfield (f=1000)_1.ffm
ylabel=s:farfield (f=1000) [1]

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Y Matrix\Y1,1
files=s:ycmplx1(1)1(1).sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Z Matrix\Z1,1
files=s:zcmplx1(1)1(1).sig

type=s:XYSIGNAL2
subtype=s:linear
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\VSWR\VSWR1
files=s:vswr1(1).sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Power\Excitation [1]\Power Radiated
files=s:RadiatedPower_1.sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Efficiencies\Rad. Efficiency [1]
files=s:FarfieldMetaData_1_RadEff.sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Efficiencies\Tot. Efficiency [1]
files=s:FarfieldMetaData_1_TotEff.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=0\farfield (f=800)
files=s:Farfield_Cut_farfield (f=800)_Phi=0_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=90\farfield (f=800)
files=s:Farfield_Cut_farfield (f=800)_Phi=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Theta=90\farfield (f=800)
files=s:Farfield_Cut_farfield (f=800)_Theta=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=0\farfield (f=900)
files=s:Farfield_Cut_farfield (f=900)_Phi=0_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=90\farfield (f=900)
files=s:Farfield_Cut_farfield (f=900)_Phi=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Theta=90\farfield (f=900)
files=s:Farfield_Cut_farfield (f=900)_Theta=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=0\farfield (f=1000)
files=s:Farfield_Cut_farfield (f=1000)_Phi=0_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Phi=90\farfield (f=1000)
files=s:Farfield_Cut_farfield (f=1000)_Phi=90_[1]_0.sig

type=s:FARFIELD1DCUT
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:Farfields\Farfield Cuts\Excitation [1]\Theta=90\farfield (f=1000)
files=s:Farfield_Cut_farfield (f=1000)_Theta=90_[1]_0.sig

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\Reference Impedance\ZRef 1(1)
files=s:ZRef1(1).sig

type=s:RESULT_0D
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:rebuild
result=s:1
treepath=s:1D Results\AutomaticRunInformation
files=s:AutomaticRunInformation

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Loss per Material\Metal loss in Brass (65%)
files=s:cMetal_loss_Brass (65%)(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Loss per Material\Metal loss in Copper (annealed)
files=s:cMetal_loss_Copper (annealed)(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:complex
problemclass=s::8:1000
visibility=s:visible
creation=s:internal
lifetime=s:surviveparchange
result=s:1
parametric=s:P
treepath=s:1D Results\Power\Excitation [1]\Loss in Metals
files=s:cTotal_metal_loss(1).sig
xlabel=s:Frequency / MHz
ylabel=s:W
title=s:Power in Watt

type=s:XYSIGNAL2
subtype=s:farfield cartesian
problemclass=s::8:1000
visibility=s:hidden
creation=s:internal
lifetime=s:solverstart
result=s:0
treepath=s:Farfields\farfield (f=900) [1]\farfield (f=900) [1]
files=s:farfield (f=900) [1].sig
xlabel=s:Phi / Degree
ylabel=s:dBi
title=s:Farfield Directivity Abs (Theta=90)

