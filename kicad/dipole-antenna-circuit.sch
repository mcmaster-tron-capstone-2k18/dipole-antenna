EESchema Schematic File Version 4
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Antenna_Dipole AE1
U 1 1 5C0F2D49
P 3850 3050
F 0 "AE1" V 3854 3030 50  0000 L CNN
F 1 "Antenna_Dipole" V 3945 3030 50  0000 L CNN
F 2 "capstone_footprints:dipole-holddown" H 3850 3050 50  0001 C CNN
F 3 "~" H 3850 3050 50  0001 C CNN
	1    3850 3050
	0    -1   -1   0   
$EndComp
$Comp
L Connector:Conn_Coaxial J1
U 1 1 5C0F2EBD
P 6650 3200
F 0 "J1" H 6580 3438 50  0000 C CNN
F 1 "Conn_Coaxial" H 6580 3347 50  0000 C CNN
F 2 "Connector_Coaxial:SMA_Amphenol_132289_EdgeMount" H 6650 3200 50  0001 C CNN
F 3 " ~" H 6650 3200 50  0001 C CNN
	1    6650 3200
	1    0    0    1   
$EndComp
$Comp
L Device:Transformer_1P_1S T1
U 1 1 5C0F318F
P 5750 3000
F 0 "T1" H 5750 3378 50  0000 C CNN
F 1 "CX2024NL" H 5750 3287 50  0000 C CNN
F 2 "capstone_footprints:CX2024NL" H 5750 3000 50  0001 C CNN
F 3 "~" H 5750 3000 50  0001 C CNN
	1    5750 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	6450 3200 6150 3200
Wire Wire Line
	6150 2800 6650 2800
Wire Wire Line
	6650 2800 6650 3000
$Comp
L Device:C C2
U 1 1 5C1A8071
P 4550 3000
F 0 "C2" H 4665 3046 50  0000 L CNN
F 1 "7.3p" H 4665 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4588 2850 50  0001 C CNN
F 3 "~" H 4550 3000 50  0001 C CNN
F 4 "CBR06C739B5GAC" H 4550 3000 50  0001 C CNN "PN"
	1    4550 3000
	-1   0    0    1   
$EndComp
$Comp
L Device:L L1
U 1 1 5C1A80E0
P 4800 2800
F 0 "L1" V 4622 2800 50  0000 C CNN
F 1 "6.8n" V 4713 2800 50  0000 C CNN
F 2 "Inductor_SMD:L_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 4800 2800 50  0001 C CNN
F 3 "~" H 4800 2800 50  0001 C CNN
F 4 "LQW18AN6N8G8ZD" V 4800 2800 50  0001 C CNN "PN"
	1    4800 2800
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 5C1A812A
P 5050 3000
F 0 "C1" H 5165 3046 50  0000 L CNN
F 1 "7.9p" H 5165 2955 50  0000 L CNN
F 2 "Capacitor_SMD:C_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 5088 2850 50  0001 C CNN
F 3 "~" H 5050 3000 50  0001 C CNN
F 4 "CBR06C799B5GAC" H 5050 3000 50  0001 C CNN "PN"
	1    5050 3000
	-1   0    0    1   
$EndComp
Wire Wire Line
	4050 3050 4050 3200
Wire Wire Line
	4050 3200 4550 3200
Wire Wire Line
	4050 2950 4050 2800
Wire Wire Line
	4050 2800 4550 2800
Wire Wire Line
	5050 3150 5050 3200
Connection ~ 5050 3200
Wire Wire Line
	5050 3200 5350 3200
Wire Wire Line
	5050 2850 5050 2800
Wire Wire Line
	5050 2800 5350 2800
Wire Wire Line
	4550 3150 4550 3200
Connection ~ 4550 3200
Wire Wire Line
	4550 2850 4550 2800
Wire Wire Line
	4650 2800 4550 2800
Connection ~ 4550 2800
Wire Wire Line
	4950 2800 5050 2800
Connection ~ 5050 2800
Wire Wire Line
	4550 3200 5050 3200
$EndSCHEMATC
